/** @format */

// ex 01
var numOfDayWork = 30;
var calcSalary = numOfDayWork * 100000;
console.log(calcSalary);

// ex 02
var num1 = 5;
var num2 = 10;
var num3 = 15;
var num4 = 25;
var num5 = 35;

var averageNumber = (num1 + num2 + num3 + num4 + num5) / 5;
console.log(averageNumber);

// ex 03
var usd = 2;
var usd2Vnd = usd * 23500;
console.log(usd2Vnd);

// ex 04

var widthRect = 10;
var hightRect = 5;
var areaRect = widthRect * hightRect;
var perimeterRect = (widthRect + hightRect) * 2;
console.log(areaRect);
console.log(perimeterRect);

// ex 05
var n = 44;
var sumN = (n % 10) + Math.floor(n / 10);
console.log(sumN);
